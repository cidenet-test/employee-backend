package com.Cidenet.Employees.Entities;

import com.Cidenet.Employees.Enums.CountriesEnum;
import com.Cidenet.Employees.Models.EmployeeReqDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Date;

@Entity
@Table(name = "employees")
@Getter
@NoArgsConstructor
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Setter
    @NotBlank(message = "Surname is required")
    @Column(length = 20)
    @Size(min = 3, max = 20, message = "20 letters max")
    @Pattern(regexp = "[A-Z]+", message = "only letter allowed without specials characters (ñ) or accents")
    private String firstSurname;

    @Setter
    @NotBlank(message = "Firstname is required")
    @Column(length = 20)
    @Size(min = 3, max = 20, message = "20 letters max")
    @Pattern(regexp = "[A-Z]+", message = "Only letter allowed without specials characters (ñ) or accents")
    private String firstName;

    @Setter
    @Column(length = 50)
    @Size(min = 3, max = 50, message = "50 letters max")
    @Pattern(regexp = "[A-Z ]+", message = "Only letter allowed without specials characters (ñ) or accents")
    private String otherNames;

    @NotNull(message = "Country is required")
    @Enumerated(EnumType.STRING)
    @Setter
    private CountriesEnum country;

    @Setter
    @NotBlank
    @Column(unique = true)
    private String email;

    @Temporal(TemporalType.TIMESTAMP)
    private final Date created = new Date();


    public Employee(EmployeeReqDto employeeReqDto) {
        this.firstSurname = employeeReqDto.getFirstSurname();
        this.firstName = employeeReqDto.getFirstName();
        this.otherNames = employeeReqDto.getOtherNames();
        this.country = CountriesEnum.valueOf(employeeReqDto.getCountry());
        this.email = "";
    }
}
