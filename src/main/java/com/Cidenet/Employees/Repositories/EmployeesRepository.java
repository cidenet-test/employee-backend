package com.Cidenet.Employees.Repositories;

import com.Cidenet.Employees.Entities.Employee;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface EmployeesRepository extends JpaRepository<Employee, Long> {
    Optional<Employee> findByEmail(String email);

    @Override
    Page<Employee> findAll(Pageable pageable);
}
