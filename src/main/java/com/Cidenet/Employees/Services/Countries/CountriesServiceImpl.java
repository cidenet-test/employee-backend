package com.Cidenet.Employees.Services.Countries;

import com.Cidenet.Employees.Enums.CountriesEnum;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CountriesServiceImpl implements CountriesService{
    @Override
    public List<String> getAllCountries() {
        return CountriesEnum.enumToString();
    }
}
