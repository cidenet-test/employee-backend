package com.Cidenet.Employees.Services.Countries;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CountriesService {
    List<String> getAllCountries();
}
