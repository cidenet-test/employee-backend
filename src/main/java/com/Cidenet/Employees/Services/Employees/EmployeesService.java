package com.Cidenet.Employees.Services.Employees;

import com.Cidenet.Employees.Entities.Employee;
import com.Cidenet.Employees.Models.EmployeeReqDto;
import com.Cidenet.Employees.Models.EmployeeResDto;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public interface EmployeesService {
    EmployeeResDto findAllEmployees(int page);

    Employee addNewEmployee(EmployeeReqDto employeeReqDto);

    Optional<Employee> removeEmployee(Long id);
}
