package com.Cidenet.Employees.Services.Employees;

import com.Cidenet.Employees.Entities.Employee;
import com.Cidenet.Employees.Enums.CountriesEnum;
import com.Cidenet.Employees.Models.EmployeeReqDto;
import com.Cidenet.Employees.Models.EmployeeResDto;
import com.Cidenet.Employees.Repositories.EmployeesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class EmployeesServiceImpl implements EmployeesService {

    @Autowired
    EmployeesRepository employeesRepository;

    @Override
    public EmployeeResDto findAllEmployees(int page) {
        EmployeeResDto resDto = new EmployeeResDto();
        Pageable pageRequest = PageRequest.of(page, 10);
        var employeesPage = employeesRepository.findAll(pageRequest);
        resDto.setDataLength(employeesPage.getTotalElements());
        resDto.setData(employeesPage.toList());
        return resDto;
    }

    @Override
    public Employee addNewEmployee(EmployeeReqDto employeeReqDto) {
        Employee employee = new Employee(employeeReqDto);
        employee.setEmail(createEmail(employee));
        return employeesRepository.save(employee);
    }

    private String createEmail(Employee employee) {
        String tdl = employee.getCountry().equals(CountriesEnum.COLOMBIA) ? "cidenet.com.co" : "cidenet.com.us" ;
        String emailName = employee.getFirstName().toLowerCase() + "." + employee.getFirstSurname().toLowerCase();
        boolean validEmail = false;
        var ending = "";
        String email = "";
        int counter = 2;
        while (!validEmail) {
            email = emailName + ending + "@" + tdl;
            var searchEmail = employeesRepository.findByEmail(email);
            if (searchEmail.isEmpty()) {
                validEmail = true;
            } else {
                ending = "." + counter;
                counter++;
            }
        }
        return email;
    }

    @Override
    public Optional<Employee> removeEmployee(Long id) {
        var employee = employeesRepository.findById(id);
        employee.ifPresent(value -> employeesRepository.delete(value));
        return employee;
    }

}
