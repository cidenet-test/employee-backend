package com.Cidenet.Employees.Config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OpenAPIConfig {
    @Bean
    public OpenAPI customOpenAPI(@Value("${application-description}") String appDescription, @Value("${application-version}") String appVersion) {

        return new OpenAPI()
                .info(new Info()
                        .title("CIDENET Employees Management")
                        .version(appVersion)
                        .description(appDescription)
                        .termsOfService("http://swagger.io/terms/"));

    }
}
