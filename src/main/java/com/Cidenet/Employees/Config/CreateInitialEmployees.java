package com.Cidenet.Employees.Config;

import com.Cidenet.Employees.Enums.CountriesEnum;
import com.Cidenet.Employees.Models.EmployeeReqDto;
import com.Cidenet.Employees.Services.Employees.EmployeesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CreateInitialEmployees implements CommandLineRunner {
    @Autowired
    private EmployeesService employeesService;

    @Override
    public void run(String... args) {
        for (EmployeeReqDto employee :
                this.getEmployess()) {
            employeesService.addNewEmployee(employee);
        }
    }

    List<EmployeeReqDto> getEmployess() {
        List<EmployeeReqDto> list = new ArrayList<>();
        list.add(new EmployeeReqDto("PEDRO","PELLICER","ANTONIO", CountriesEnum.COLOMBIA.toString()));
        list.add(new EmployeeReqDto("MIGUEL","RAMOS","RIVERO", CountriesEnum.ESTADOSUNIDOS.toString()));
        list.add(new EmployeeReqDto("HECTOR","BALAGUER","PINEIRO", CountriesEnum.COLOMBIA.toString()));
        list.add(new EmployeeReqDto("ELIANA","CUEVAS","MARIA", CountriesEnum.COLOMBIA.toString()));
        list.add(new EmployeeReqDto("PILI","MAESTRE","DE LOS RIOS", CountriesEnum.COLOMBIA.toString()));
        list.add(new EmployeeReqDto("VIVIANA","GRANADOS","MARIA", CountriesEnum.ESTADOSUNIDOS.toString()));
        list.add(new EmployeeReqDto("PATRICIA","PELAYO","URRUTIA", CountriesEnum.ESTADOSUNIDOS.toString()));
        list.add(new EmployeeReqDto("ADELA","PALOMINO","CONZTANZA", CountriesEnum.COLOMBIA.toString()));
        list.add(new EmployeeReqDto("ISIDORO","ROBLEDO","OLIVARES", CountriesEnum.ESTADOSUNIDOS.toString()));
        list.add(new EmployeeReqDto("LUIS","MIRO","MIGUEL", CountriesEnum.COLOMBIA.toString()));
        list.add(new EmployeeReqDto("JAIME","TUDELA","GONIO", CountriesEnum.ESTADOSUNIDOS.toString()));
        list.add(new EmployeeReqDto("LUISINA","PASCUAL","DE LA TORRE", CountriesEnum.COLOMBIA.toString()));
        list.add(new EmployeeReqDto("GERARDO","ASENSIO","FERRANDO", CountriesEnum.ESTADOSUNIDOS.toString()));
        list.add(new EmployeeReqDto("ANDREA","NOGUEIRA","FLORIDA", CountriesEnum.COLOMBIA.toString()));
        list.add(new EmployeeReqDto("HORTENSIA","MORATA","VALENTINA", CountriesEnum.ESTADOSUNIDOS.toString()));
        list.add(new EmployeeReqDto("RITA","IRIARTE","PILAR", CountriesEnum.COLOMBIA.toString()));
        list.add(new EmployeeReqDto("RUBEN","FUENTE","PEREIRA", CountriesEnum.ESTADOSUNIDOS.toString()));
        list.add(new EmployeeReqDto("RODOLFO","CABEZAS","ROJAS", CountriesEnum.ESTADOSUNIDOS.toString()));
        list.add(new EmployeeReqDto("ADRIAN","MANSO","VENDRELL", CountriesEnum.COLOMBIA.toString()));
        list.add(new EmployeeReqDto("LETICIA","CASADO","DEL PRADO", CountriesEnum.COLOMBIA.toString()));
        list.add(new EmployeeReqDto("LEANDRO","PRADO","PALOMARES", CountriesEnum.COLOMBIA.toString()));
        list.add(new EmployeeReqDto("FITO","GALAN","VALBUENA", CountriesEnum.ESTADOSUNIDOS.toString()));
        list.add(new EmployeeReqDto("TONI","RIVERO","CABANILLAS", CountriesEnum.ESTADOSUNIDOS.toString()));
        list.add(new EmployeeReqDto("RENATO","SANTAMARIA","FIGUEROA", CountriesEnum.COLOMBIA.toString()));
        list.add(new EmployeeReqDto("MACARIO","ALAMO","SARABIA", CountriesEnum.COLOMBIA.toString()));
        return list;
    }
}
