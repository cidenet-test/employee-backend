package com.Cidenet.Employees.Controllers;

import com.Cidenet.Employees.Entities.Employee;
import com.Cidenet.Employees.Models.EmployeeReqDto;
import com.Cidenet.Employees.Models.EmployeeResDto;
import com.Cidenet.Employees.Services.Employees.EmployeesService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(path = "/api/v1/employees", produces = APPLICATION_JSON_VALUE)
public class EmployeesController {

    private static final Logger logger = LoggerFactory.getLogger(EmployeesController.class);
    private static final String NEW_EMPLOYEE_LOG = "New employee was created: {}";
    private static final String EMPLOYEE_DELETED_LOG = "Employee: {} was updated";
    @Autowired
    EmployeesService employeesService;

    @Operation(summary = "Returns a list of employees with a limit of 10 per page")
    @ApiResponse(responseCode = "200", description = "List of employees and total count of them", content = {@Content(mediaType = APPLICATION_JSON_VALUE, schema = @Schema(implementation = EmployeeResDto.class))})
    @ApiResponse(responseCode = "204", description = "No employees in database", content = @Content)
    @GetMapping
    public ResponseEntity<EmployeeResDto> getAllEmployees(@RequestParam(required = false, name = "page", defaultValue = "0") int page) {
        var employeeList = employeesService.findAllEmployees(page);
        if (employeeList.getData().isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(employeeList);
    }

    @Operation(summary = "Create a new employee")
    @ApiResponse(responseCode = "201", description = "Employee created", content = {@Content(mediaType = APPLICATION_JSON_VALUE, schema = @Schema(implementation = Employee.class))})
    @ApiResponse(responseCode = "400", description = "Bad request, verify the data sent", content = @Content)
    @PostMapping(consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<Employee> createEmployee(@Valid @RequestBody EmployeeReqDto employeeReqDto) {
        var employee = employeesService.addNewEmployee(employeeReqDto);
        logger.info(NEW_EMPLOYEE_LOG, employee.toString());
        return ResponseEntity.status(HttpStatus.CREATED).body(employee);
    }

    @Operation(summary = "Delete an employee")
    @ApiResponse(responseCode = "200", description = "Employee deleted", content = {@Content(mediaType = APPLICATION_JSON_VALUE, schema = @Schema(implementation = Employee.class))})
    @ApiResponse(responseCode = "404", description = "Employee not found", content = @Content)
    @DeleteMapping("/{id}")
    public ResponseEntity<Employee> deleteEmployee(@PathVariable Long id) {
        var employee = employeesService.removeEmployee(id);
        if (employee.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        logger.info(EMPLOYEE_DELETED_LOG, employee.get());
        return ResponseEntity.ok().body(employee.get());
    }
}
