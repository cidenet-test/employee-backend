package com.Cidenet.Employees.Controllers;

import com.Cidenet.Employees.Services.Countries.CountriesService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(path = "/api/v1/countries", produces = APPLICATION_JSON_VALUE)
public class CountriesController {

    @Autowired
    CountriesService countriesService;

    @Operation(summary = "Returns a list of countries where Cidenet is present")
    @ApiResponse(responseCode = "200", description = "List of countries", content = {@Content(mediaType = APPLICATION_JSON_VALUE)})
    @GetMapping
    public ResponseEntity<List<String>> getAllCountries() {
        return ResponseEntity.ok(countriesService.getAllCountries());
    }
}
