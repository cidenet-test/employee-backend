package com.Cidenet.Employees.Models;

import lombok.Getter;
import lombok.Setter;


import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Getter
@Setter
public class EmployeeReqDto {
    @NotBlank(message = "El apellido es requerido")
    @Size(min = 3, max = 20, message = "Máximo 20 letras")
    @Pattern(regexp = "[A-Z]+", message = "Solo letras mayúsculas, sin acentos ó caracteres especiales (ñ)")
    private String firstSurname;

    @NotBlank(message = "El nombre es requerido")
    @Size(min = 3, max = 20, message = "Máximo 20 letras")
    @Pattern(regexp = "[A-Z]+", message = "Solo letras mayúsculas, sin acentos ó caracteres especiales (ñ)")
    private String firstName;

    @Size(max = 50, message = "Máximo 50 letras")
    @Pattern(regexp = "[A-Z ]+", message = "Solo letras mayúsculas, sin acentos ó caracteres especiales (ñ)")
    private String otherNames;

    @NotBlank(message = "El país es requerido")
    @Pattern(regexp = "(COLOMBIA|ESTADOSUNIDOS)+", message = "Las opciones válidas son COLOMBIA ó ESTADOSUNIDOS")
    private String country;

    public EmployeeReqDto(String firstSurname, String firstName, String otherNames, String country) {
        this.firstSurname = firstSurname;
        this.firstName = firstName;
        this.country = country;
        this.otherNames = otherNames;
    }
}
