package com.Cidenet.Employees.Models;

import com.Cidenet.Employees.Entities.Employee;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class EmployeeResDto {
    private List<Employee> data;
    private Long dataLength;
}
