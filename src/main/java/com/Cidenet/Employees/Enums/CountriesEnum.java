package com.Cidenet.Employees.Enums;

import java.util.ArrayList;
import java.util.List;

public enum CountriesEnum {
    COLOMBIA, ESTADOSUNIDOS;

    public static List<String> enumToString() {
        List<String> values = new ArrayList<>();
        for (CountriesEnum value : CountriesEnum.values()) {
            values.add(value.name());
        }
        return values;
    }
}
