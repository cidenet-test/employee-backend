# Employee Backend

API REST creada en spring boot para el manejo de empleados, preparada para comunucarse con cualquier 
aplicación front, sea movil o web, se creó un front en angular para interactuar con el API Rest al cual 
se puede llegar dando click [aquí](https://gitlab.com/cidenet-test/employee-frontend).

Esta API está configurada para utilizar la base de datos en memoria H2, por lo que no es necesario
configurar base de datos para hacer pruebas.


## Instalación

Se debe clonar este repositorio en el equipo donde se vaya a instalar para continuar con el desarrollo,
mediante la instrucción

```bash
git clone https://gitlab.com/cidenet-test/employee-backend.git
```

Acceda al a carpeta del proyecto recién clonado

```bash
cd employee-backend
```

Para correr en un ambiente de desarrollo se recomienda usar el IDE IntelliJ que descarga las dependencias 
necesarias para correr el proyecto

## Uso

Mediante la terminal ejecute el siguiente comando 

```bash
mvn spring-boot:run
```
El proyecto quedará ejecutándose y el API escuchará peticiones el puerto 9003


## Consola H2

Para acceder a la consola de la base de datos en memoria H2 en la direccion

http://localhost:9003/h2-console


## Documentación Swagger

El API está documentada y se puede acceder desde direeción

http://localhost:9003/swagger-ui/index.html

